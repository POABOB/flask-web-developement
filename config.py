import os
basedir = os.path.abspath(os.path.dirname(__file__))


class BaseConfig:  # 基本配置类
    # CSRF Token
    SECRET_KEY = os.getenv('SECRET_KEY') or '1ust j3rk m3 077'

    # SSL_DISABLE = False
    # SQLALCHEMY_COMMIT_ON_TEARDOWN = True
    # SQLALCHEMY_RECORD_QUERIES = True

    MAIL_SERVER = os.environ.get('MAIL_SERVER', 'smtp.googlemail.com')
    MAIL_PORT = int(os.environ.get('MAIL_PORT', '587'))
    MAIL_USE_TLS = True
    MAIL_USE_SSL = False
    MAIL_USERNAME = os.environ.get('MAIL_USERNAME') or 'nuic2019ncue@gmail.com'
    MAIL_PASSWORD = os.environ.get('MAIL_PASSWORD') or 'yiwuskwnjscsumsm'
    FLASKY_MAIL_SUBJECT_PREFIX = '[Flasky]'
    FLASKY_MAIL_SENDER = 'Flasky Admin <flasky@example.com>'
    FLASKY_ADMIN = 'zxc752166@gmail.com'

    # 頁數
    FLASKY_POSTS_PER_PAGE = 10
    FLASKY_FOLLOWERS_PER_PAGE = 50
    FLASKY_COMMENTS_PER_PAGE = 30
    FLASKY_SLOW_DB_QUERY_TIME = 0.5

    @staticmethod
    def init_app(app):
        pass
class DevelopmentConfig(BaseConfig):
    DEBUG = True
    # 預設mysql所需配置
    SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://root:mysql@localhost:3306/Mydb'
    # 每次請求結束後 會自動提交資料庫中的異動
    SQLALCHEMY_TRACK_MODIFICATIONS = True
    # 顯示已使用的語句在terminal中
    SQLALCHEMY_ECHO = True
class TestingConfig(BaseConfig):
    TESTING = True
    WTF_CSRF_ENABLED = False

# config參數設定
config = {
    'development': DevelopmentConfig,
    'testing': TestingConfig,
    'default': DevelopmentConfig,
}