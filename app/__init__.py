from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from config import config
from flask_moment import Moment
from flask_bootstrap import Bootstrap
from flask_mail import Mail
from flask_login import LoginManager
# ==================================介面類==========================================
bootstrap = Bootstrap()

moment = Moment()

db = SQLAlchemy()

mail = Mail()

login_manager = LoginManager()
login_manager.session_protection = 'strong'
login_manager.login_view = 'auth.login'

def create_app(config_name):
    # 初始化flask物件
    # 需傳遞一參數:__name__
    # 1.方便Flask框架尋找資源
    # 2.方便除錯
    app = Flask(__name__)
    # 引入config中相對應的參數
    app.config.from_object(config[config_name])

    config[config_name].init_app(app)
    bootstrap.init_app(app)
    moment.init_app(app)
    db.init_app(app)
    mail.init_app(app)
    login_manager.init_app(app)

    from .main import main as main_blueprint
    app.register_blueprint(main_blueprint)

    from .auth import auth as auth_blueprint
    app.register_blueprint(auth_blueprint, url_prefix='/auth')
    # 附加路由何自定義錯誤頁面
    return app