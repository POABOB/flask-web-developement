from flask_wtf import Form
from wtforms import StringField, PasswordField, BooleanField, SubmitField, TextAreaField
from wtforms.validators import Required, Length, Email, Regexp, EqualTo, DataRequired
from wtforms import ValidationError
from ..models import User

class LoginForm(Form):
    email = StringField('Email', validators = [Required(), Length(5, 66), Email()])
    password = PasswordField('密碼', validators = [Required()])
    remember_me = BooleanField('保持登入')
    submit = SubmitField('登入')

class RegistrationForm(Form):
    email = StringField('Email', validators = [Required(), Length(5, 66), Email()])
    username = StringField('ID', validators = [Required(), Length(1, 66)])
    password = PasswordField('密碼', validators=[Required(), EqualTo('password2', message = '密碼必須一致!')])
    password2 = PasswordField('密碼確認', validators=[Required()])
    submit = SubmitField('註冊')

    def validate_email(self, field):
        if User.query.filter_by(email = field.data).first():
            raise ValidationError('Email 已經被註冊囉!')

    def validate_username(self, field):
        if User.query.filter_by(username = field.data).first():
            raise ValidationError('ID 已經被註冊囉!')

class ChangePasswordForm(Form):
    old_password = PasswordField('舊的密碼', validators=[Required()])
    password = PasswordField('新的密碼', validators=[Required(), EqualTo('password2', message = '密碼必須一致!')])
    password2 = PasswordField('密碼確認', validators=[Required()])
    submit = SubmitField('更改密碼')

class PasswordResetRequestForm(Form):
    email = StringField('Email', validators=[DataRequired(), Length(5, 66),Email()])
    submit = SubmitField('重設密碼')

class PasswordResetForm(Form):
    password = PasswordField('新的密碼', validators=[Required(), EqualTo('password2', message='密碼必須一致!')])
    password2 = PasswordField('密碼確認', validators=[Required()])
    submit = SubmitField('更改密碼')

