from flask import render_template, session, redirect, url_for, flash, abort
from datetime import datetime
from . import main
from .forms import NameForm, EditProfileForm, EditProfileAdminForm
from .. import db
from ..models import User, Role
from flask_login import login_required, current_user
from ..decorators import admin_required
# -------------------------------------首頁----------------------------------------
# 在函數上方，@為裝飾器
# 用途為作一url與視圖映射 127.0.0.1:5000/
@main.route('/', methods = ['GET', 'POST'])
def index():
    form = NameForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username = form.name.data).first()
        if user is None:
            user = User(username = form.name.data)
            db.session.add(user)
            db.session.commit()
            session['known'] = False
        else:
            session['known'] = True
        if session['known'] is False:
            flash('沒人有使用這個名字唷!')

        session['name'] = form.name.data
        form.name.data = ''
        return redirect(url_for('.index'))
    return render_template('/index/index.html', form=form, name=session.get('name'), known = session.get('known', False), current_time=datetime.utcnow())


# ------------------------------------------------------------------------------------
@main.route('/user/<username>/')
def user(username):
    user = User.query.filter_by(username = username).first()
    if user is None:
        abort(404)
    return render_template('/user/user.html', user = user)

@main.route('/edit_profile', methods=['GET', 'POST'])
@login_required
def edit_profile():
    form = EditProfileForm()
    if form.validate_on_submit():
        current_user.name = form.name.data
        current_user.location = form.location.data
        current_user.about_me = form.about_me.data
        db.session.add(current_user)
        db.session.commit()
        flash('你的個人資訊已經更新完畢!')
        return redirect(url_for('main.user', username=current_user.username))
    form.name.data = current_user.name
    form.location.data = current_user.location
    form.about_me.data = current_user.about_me
    return render_template('user/edit_profile.html', form=form)


@main.route('/edit_profile/<int:id>', methods=['GET', 'POST'])
@login_required
@admin_required
def edit_profile_admin(id):
    user = User.query.get_or_404(id)
    form = EditProfileAdminForm(user=user)
    if form.validate_on_submit():
        user.email = form.email.data
        user.username = form.username.data
        user.confirmed = form.confirmed.data
        user.role = Role.query.get(form.role.data)
        user.name = form.name.data
        user.location = form.location.data
        user.about_me = form.about_me.data
        db.session.add(user)
        db.session.commit()
        flash('個人資訊已經更新完畢!')
        return redirect(url_for('main.user', username=user.username))
    form.email.data = user.email
    form.username.data = user.username
    form.confirmed.data = user.confirmed
    form.role.data = user.role_id
    form.name.data = user.name
    form.location.data = user.location
    form.about_me.data = user.about_me
    return render_template('user/edit_profile.html', form=form, user=user)


# =====================================功能類=======================================

# =====================================啟動面=======================================
# 如果當前這個文件作為入口程式運行，那就run app.run()
if __name__ == '__main__':
    # 啟動一個server 使用戶連接
    # listen()
    main.run()
