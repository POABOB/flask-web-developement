from flask_wtf import Form
from wtforms import StringField, SubmitField, TextAreaField, BooleanField, SelectField, ValidationError
from wtforms.validators import Required, Length, Email
from ..models import Role, User

# ===================================表單類=========================================
class NameForm(Form):
    name = StringField('What\'s your name?', validators = [Required()])
    submit = SubmitField('Submit')

class EditProfileForm(Form):
    name = StringField('真實姓名', validators=[Length(1, 66)])
    location = StringField('城市', validators=[Length(1, 66)])
    about_me = TextAreaField('關於我', default='''這人好懶，甚麼都沒有寫OwO''')
    submit = SubmitField('送出')

class EditProfileAdminForm(Form):
    email = StringField('Email', validators=[Required(), Length(5, 66), Email()])
    username = StringField('ID', validators=[Required(), Length(1, 64), ])
    confirmed = BooleanField('是否驗證')
    role = SelectField('角色', coerce=int)
    name = StringField('真實姓名', validators=[Length(1, 66)])
    location = StringField('城市', validators=[Length(1, 66)])
    about_me = TextAreaField('關於我', default='''這人好懶，甚麼都沒有寫OwO''')
    submit = SubmitField('送出')

    def __init__(self, user, *args, **kwargs):
        super(EditProfileAdminForm, self).__init__(*args, **kwargs)
        self.role.choices = [(role.id, role.name) for role in Role.query.order_by(Role.name).all()]
        self.user = user

    def validate_email(self, field):
        if field.data != self.user.email and User.query.filter_by(email=field.data).first():
            raise ValidationError('Email已經被註冊過了')

    def validate_username(self, field):
        if field.data != self.user.username and User.query.filter_by(username=field.data).first():
            raise ValidationError('ID已經被使用過了')