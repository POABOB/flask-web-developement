from flask import render_template
from . import main

# ====================================頁面類========================================
# ---------------------------------自定義錯誤頁面-------------------------------------
@main.app_errorhandler(404)
def page_not_found(e):
    return render_template('base/404.html'), 400

@main.app_errorhandler(500)
def internal_server_error(e):
    return render_template('base/500.html'), 500

@main.app_errorhandler(403)
def forbidden(e):
    return render_template('base/403.html'), 403
